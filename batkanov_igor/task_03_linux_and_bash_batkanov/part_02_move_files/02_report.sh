#!/bin/bash
show_help () {
   echo
   echo " Script usage:"
   echo " ./<script> <Result_Directory>"
   echo
   echo "   <Result_Directory>   - directory with processed files"
   echo
exit 0
}

check_slash () {
   dirToCheck=$1
   length=${#dirToCheck}
   last_char=${dirToCheck:length-1:1}
   if [ $last_char != "/" ]; then
     dirWithSlash=$(echo "$dirToCheck/")
   echo $dirWithSlash
   else
     echo $1
   fi
}

check_if_directory_exist () {
   echo
   echo "Checking provided paths:"
   if [ -d $1 ]; then
     echo
     echo "OK - Provided directory $1 exist!"
   else
     echo
     echo "NOT OK - Provided directory $1 doesn't exist! Please crate it!"
   fi
}

check_passed_arguments () {
     if [ -z $resultDir ]; then
        echo
        echo "No result dir supplied"
        show_help
        exit 0
     fi
}

create_report () {
   objectsArray=($(ls $1 | awk -F '-' '{print $1}' | sort -u))
   echo
   for i in "${objectsArray[@]}"; do
     filesCount=$(ls $resultDir$i* | wc -l)
     startDate=$(ls $resultDir$i* |awk -F '-' '{print $4"-"$5}' | sort -u | head -n 1)
     endDate=$(ls $resultDir$i* |awk -F '-' '{print $4"-"$5}' | sort -u | tail -n 1)
     echo "From $startDate to $endDate $filesCount received from $i object"
   done
}


custom_report () {
   echo
   echo "Do you want create report for another period?"
   while true; do
   read -p "Yes / No (type y or n): " -n 1 -r
   echo

   if [[ $REPLY =~ ^[y]$ ]]; then
     while true; do
     echo
     read -p "Enter start date, format is <year><month><day> (for example 20180101): " customStartDate
     if [ -z $customStartDate ]; then
       echo "You provided an empty date!"
     elif [ ${#customStartDate} -le 7 ]; then
       echo "The date provided in incorrect format"
     else
       Y=$(echo $customStartDate | cut -c 1-4)
       M=$(echo $customStartDate | cut -c 5-6)
       D=$(echo $customStartDate | cut -c 7-8)
       if date -d "$Y$M$D" &> /dev/null; then
         echo "$customStartDate is valid date"
         break
       else
         echo "$customStartDate is invalid date"
       fi
     fi
     done

     while true; do
     echo
     read -p "Enter end date, format is <year><month><day> (for example 20180102): " customEndDate
     if [ -z $customEndDate ]; then
       echo "You provided an empty date!"
     elif [ ${#customEndDate} -le 7 ]; then
       echo "The date provided in incorrect format"
     else
       Y=$(echo $customEndDate | cut -c 1-4)
       M=$(echo $customEndDate | cut -c 5-6)
       D=$(echo $customEndDate | cut -c 7-8)
       if date -d "$Y$M$D" &> /dev/null; then
         echo "$customEndDate is valid date"
         break
       else
         echo "$customEndDate is invalid date"
       fi
     fi
     done

     echo
     echo "Count of fully received and moved files per object from $customStartDate to $customEndDate: "
     customStartDateEpoch=$(date -d $customStartDate +%s)
     customEndDateEpoch=$(date -d $customEndDate +%s)

     filesCount=0
     echo
     for i in "${objectsArray[@]}"; do
       objectDates=($(ls $resultDir$i* |  awk -F '-' '{print $4}'))
       for y in "${objectDates[@]}"; do
         fileDateEpoch=$(date -d $y +%s)
         if [[ $fileDateEpoch -ge $customStartDateEpoch ]] && [[ $fileDateEpoch -le $customEndDateEpoch ]]; then
         let filesCount=$((filesCount + 1))
         fi
       done
     echo "$i: $filesCount"
     filesCount=0
     done
     break;
   fi

   if [[ $REPLY =~ ^[n]$ ]]; then
   exit 0
   fi
   done
}

resultDir=$1
check_passed_arguments $resultDir
resultDir=$(check_slash $resultDir)
check_if_directory_exist $resultDir
create_report $resultDir
custom_report
