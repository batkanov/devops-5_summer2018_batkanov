# Task 03: Linux & BASH
Студент: Игорь Батьканов.

### 1. Скрипт создания конфигурационного файла из шаблона

```sh
part_01_create_config_from_template/01_create_config.sh
```

### 2. Скрипт переноса файлов
```sh
part_02_move_files/01_create_config.sh
```

Дополнительно:

Сделать отчет сколько файлов от какого объекта было получено за заданный промежуток времени
```sh
part_02_move_files/02_report.sh
```

Сделать возможность архивации результирующих фалов, которые находились в результирующий директории больше определенного времени. Архивы компоновать по имени объектов. В названии архива также должен быть указан за какой период в нем хранятся файлы.

```sh
part_02_move_files/03_archive_files.sh
```


### 3. Установка MySQL'я
Я использовал yum
```sh
part_03_mysql_setup/01_setup.sh
```

Иформацию об использовании любого из скриптов можно получить просто запустив его без каких-либо параметров
```
[user@batkanov-b71cb2123 part_01_create_config_from_template]# ./01_create_config.sh

 Script usage:
 ./<script> <Template_File> <Result_File>

   <Template_File>   - template file
   <Result_File>     - configuration file which will be created
```
